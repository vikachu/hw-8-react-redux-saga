import React from "react";
import "../styles/Copyright.scss";

const Copyright = () => {
  return <div className="copyright__container">Copyright</div>;
};

export default Copyright;
