import React from "react";
import PropTypes from "prop-types";
import "../styles/MessageInput.scss";

const MessageInput = ({
  onMessageInputChange,
  inputValue,
  onSendMessageClick,
}) => {
  return (
    <div className="message-input__container">
      <textarea
        className="message-input__text"
        placeholder="Message"
        onChange={onMessageInputChange}
        value={inputValue}
      ></textarea>

      <button className="message-input__button" onClick={onSendMessageClick}>
        Send
      </button>
    </div>
  );
};

MessageInput.propTypes = {
  onMessageInputChange: PropTypes.func.isRequired,
  onSendMessageClick: PropTypes.func.isRequired,
  inputValue: PropTypes.string,
};

export default MessageInput;
