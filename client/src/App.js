import React from "react";
import { Switch, Route } from "react-router-dom";

import Chat from "./components/Chat";
import EditMessageForm from "./components/EditMessageForm";
import UserList from "./components/UserList";
import UserForm from "./components/UserForm";
import Login from "./components/Login";

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/messages" component={Chat} />
        <Route path="/messages/:id" component={EditMessageForm} />
        <Route exact path="/users" component={UserList} />
        <Route exact path="/users/add" component={UserForm} />
        <Route path="/users/:id" component={UserForm} />
        <Route exact path="/login" component={Login} />
      </Switch>
    </div>
  );
}

export default App;
