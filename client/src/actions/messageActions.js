import { actionTypes } from "./messageActionTypes";

export const fetchMessages = () => ({
  type: actionTypes.FETCH_MESSAGES,
});

export const sendMessage = (data) => ({
  type: actionTypes.SEND_MESSAGE,
  payload: {
    data,
  },
});

export const likeMessage = (id) => ({
  type: actionTypes.LIKE_MESSAGE,
  payload: {
    id: id,
  },
});

export const editMessage = (id, data) => ({
  type: actionTypes.EDIT_MESSAGE,
  payload: {
    id: id,
    data,
  },
});

export const deleteMessage = (id) => ({
  type: actionTypes.DELETE_MESSAGE,
  payload: {
    id,
  },
});
