import { actionTypes } from "./userFormActionTypes";

export const fetchUser = (id) => ({
  type: actionTypes.FETCH_USER,
  payload: {
    id,
  },
});
