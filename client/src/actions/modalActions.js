import { actionTypes } from "./modalActionTypes";

export const fetchMessage = (id) => ({
  type: actionTypes.FETCH_MESSAGE,
  payload: {
    id,
  },
});
