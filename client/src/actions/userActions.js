import { actionTypes } from "./userActionTypes";

export const fetchUsers = () => ({
  type: actionTypes.FETCH_USERS,
});

export const addUser = (data) => ({
  type: actionTypes.ADD_USER,
  payload: {
    data,
  },
});

export const editUser = (id, data) => ({
  type: actionTypes.EDIT_USER,
  payload: {
    id: id,
    data,
  },
});

export const deleteUser = (id) => ({
  type: actionTypes.DELETE_USER,
  payload: {
    id,
  },
});

export const loginUser = (email, password) => ({
  type: actionTypes.LOGIN_USER,
  payload: {
    email,
    password,
  },
});
