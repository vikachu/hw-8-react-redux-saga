import axios from "axios";
import { call, put, takeEvery, all } from "redux-saga/effects";
import { actionTypes } from "../actions/messageActionTypes";
import { API_URL } from "../config";

export function* fetchMessages() {
  try {
    const response = yield call(axios.get, `${API_URL}/messages`);
    const messages = response.data;

    yield put({ type: "FETCH_MESSAGES_SUCCESS", payload: { messages } });
  } catch (error) {
    alert(`fetchMessages error: ${error.message}`);
  }
}

function* watchFetchMessages() {
  yield takeEvery(actionTypes.FETCH_MESSAGES, fetchMessages);
}

export function* sendMessage(action) {
  const newMessage = action.payload.data;

  try {
    yield call(axios.post, `${API_URL}/messages`, newMessage);
    yield put({ type: "FETCH_MESSAGES" });
  } catch (error) {
    alert(`sendMessage error: ${error.message}`);
  }
}

function* watchSendMessage() {
  yield takeEvery(actionTypes.SEND_MESSAGE, sendMessage);
}

export function* editMessage(action) {
  const id = action.payload.id;
  const message = action.payload.data;

  try {
    yield call(axios.put, `${API_URL}/messages/${id}`, message);
    yield put({ type: "FETCH_MESSAGES" });
  } catch (error) {
    alert(`editMessage error: ${error.message}`);
  }
}

function* watchEditMessage() {
  yield takeEvery(actionTypes.EDIT_MESSAGE, editMessage);
}

export function* deleteMessage(action) {
  const id = action.payload.id;

  try {
    yield call(axios.delete, `${API_URL}/messages/${id}`);
    yield put({ type: "FETCH_MESSAGES" });
  } catch (error) {
    alert(`deleteMessage error: ${error.message}`);
  }
}

function* watchDeleteMessage() {
  yield takeEvery(actionTypes.DELETE_MESSAGE, deleteMessage);
}

export function* likeMessage(action) {
  const id = action.payload.id;

  try {
    yield call(axios.post, `${API_URL}/messages/${id}/like`);
    yield put({ type: "FETCH_MESSAGES" });
  } catch (error) {
    alert(`likeMessage error: ${error.message}`);
  }
}

function* watchLikeMessage() {
  yield takeEvery(actionTypes.LIKE_MESSAGE, likeMessage);
}

export default function* messagesSagas() {
  yield all([
    watchFetchMessages(),
    watchSendMessage(),
    watchDeleteMessage(),
    watchEditMessage(),
    watchLikeMessage(),
  ]);
}
