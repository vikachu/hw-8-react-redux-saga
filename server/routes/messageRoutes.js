import { Router } from "express";
import * as messageService from "../services/messageService";

const router = Router();

router.get("/", (req, res, next) => {
  const messages = messageService.getMessages();
  res.send(messages);
});

router.get("/:id", (req, res, next) => {
  try {
    const message = messageService.getMessageById(req.params.id);
    res.send(message);
  } catch {
    next();
  }
});

router.post("/", (req, res, next) => {
  const message = messageService.createMessage(req.body);
  res.send(message);
});

router.post("/:id/like", (req, res, next) => {
  const message = messageService.likeMessage(req.params.id);
  res.send(message);
});

router.put("/:id", (req, res, next) => {
  const message = messageService.updateMessage(req.params.id, req.body);
  res.send(message);
});

router.delete("/:id", (req, res, next) => {
  const message = messageService.deleteMessage(req.params.id);
  res.send(message);
});

export default router;
