import MessageRepository from "../repositories/messageRepository";

export const getMessages = () => MessageRepository.getAll();

export const getMessageById = (id) =>
  MessageRepository.getOne((message) => message.id === id);

export const createMessage = (message) =>
  MessageRepository.create({
    ...message,
    likes: 0,
  });

export const updateMessage = (id, data) => MessageRepository.update(id, data);

export const deleteMessage = (id) => MessageRepository.delete(id);

export const likeMessage = (id) => {
  const message = MessageRepository.getOne((message) => message.id === id);
  const updateData = {
    likes: message.likes + 1,
  };
  return MessageRepository.update(id, updateData);
};
