import path from "path";
import low from "lowdb";
import FileSync from "lowdb/adapters/FileSync";

const dbPath = `${path.resolve()}/database.json`;
const adapter = new FileSync(dbPath);
const dbAdapter = low(adapter);

const defaultDb = {
  messages: [
    {
      id: "80f08600-1b8f-11e8-9629-c7eca82aa7bd",
      userId: "9e243930-83c9-11e9-8e0c-8f1a686f4ce4",
      avatar:
        "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
      user: "Ruth",
      text: "I don’t *** understand. It's the Panama accounts",
      likes: 0,
      createdAt: "2020-07-16T19:48:12.936Z",
      editedAt: "",
    },
    {
      id: "80e00b40-1b8f-11e8-9629-c7eca82aa7bd",
      userId: "533b5230-1b8f-11e8-9629-c7eca82aa7bd",
      avatar:
        "https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng",
      user: "Wendy",
      text: "Tells exactly what happened.",
      likes: 0,
      createdAt: "2020-07-16T19:48:42.481Z",
      editedAt: "2020-07-16T19:48:47.481Z",
    },
  ],
  users: [
    {
      id: "9e243930-83c9-11e9-8e0c-8f1a686f4ce4",
      avatar:
        "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
      user: "Ruth",
      email: "admin",
      password: "admin",
      isAdmin: true,
      createdAt: "2020-07-16T19:48:12.936Z",
      editedAt: "",
    },
    {
      id: "533b5230-1b8f-11e8-9629-c7eca82aa7bd",
      avatar:
        "https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng",
      user: "Wendy",
      email: "wendy@gmail.com",
      password: "password",
      isAdmin: false,
      createdAt: "2020-07-16T19:48:42.481Z",
      editedAt: "2020-07-16T19:48:47.481Z",
    },
  ],
};

dbAdapter.defaults(defaultDb).write();

export default dbAdapter;
